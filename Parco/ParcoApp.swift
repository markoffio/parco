//
//  ParcoApp.swift
//  Parco
//
//  Created by Marco Margarucci on 03/08/21.
//

import SwiftUI
import Firebase

@main
struct ParcoApp: App {
    // Persistent controller
    let persistenceController = PersistenceController.shared
    
    init() {
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            //SignupView()
                //.environment(\.managedObjectContext, persistenceController.container.viewContext)
            MainView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
