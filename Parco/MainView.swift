//
//  MainView.swift
//  Parco
//
//  Created by Marco Margarucci on 04/08/21.
//

import SwiftUI
import CoreData

struct MainView: View {
    
    // MARK: - Properties
    // Show add new credit card form
    @State private var showAddNewCreditCardForm: Bool = false
    // Show add new transaction form
    @State private var showAddNewTransactionForm: Bool = false
    // Card selection index
    @State private var cardSelectionIndex: Int = 0
    // Selected card hash
    @State private var selectedCardHash: Int = -1
    // View context
    @Environment(\.managedObjectContext) private var viewContext
    
    // Fetch cards
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Card.timestamp, ascending: false)],
        animation: .default)
    private var cards: FetchedResults<Card>
    
    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    // Title
                    Text("Your cards")
                        .font(.custom("AvenirNext-DemiBold", size: 18))
                        .foregroundColor(Color("clearBlue"))
                    Spacer()
                    // Add new card button
                    RoundedBarButton(imageName: "plus") {
                        showAddNewCreditCardForm.toggle()
                    }
                }
                .padding([.leading, .trailing], 20)
                .padding(.top, 10)
                ScrollView(showsIndicators: false) {
                    if !cards.isEmpty {
                        TabView(selection: $selectedCardHash) {
                            ForEach(cards) { card in
                                CardView(card: card)
                                    .padding(.bottom, 40)
                                    .tag(card.hash)
                            }
                        }
                        .tabViewStyle(PageTabViewStyle(indexDisplayMode: .always))
                        .frame(height: 280)
                        .id(cards.count)
                        .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
                        .onAppear {
                            self.selectedCardHash = cards.first?.hash ?? -1
                        }
                        // Add transaction button
                        RoundedButton(text: "ADD TRANSACTION") {
                            showAddNewTransactionForm.toggle()
                        }
                        .padding([.leading, .trailing], 16)
                        .fullScreenCover(isPresented: $showAddNewTransactionForm, content: {
                            if let selectedCard = cards[cardSelectionIndex] {
                                AddTransactionForm(card: selectedCard)
                            }
                        })
                        .padding(.bottom, 20)
                        // Transactions list view
                        if let firstIndex = cards.firstIndex(where: { $0.hash == selectedCardHash }) {
                            let card = self.cards[firstIndex]
                            TransactionsListView(card: card)
                        }
                    } else {
                        NoCardsMessageView()
                    }
                }
                .navigationBarHidden(true)
                .ignoresSafeArea(edges: .bottom)
            }
        }
        // Show add new credit card view
        .fullScreenCover(isPresented: $showAddNewCreditCardForm) {
            AddNewCardForm(card: nil) { card in
                self.selectedCardHash = card.hash
            }
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
            .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}

struct NoCardsMessageView: View {
    var body: some View {
        VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 16, content: {
            Spacer()
            Image("fear")
                .resizable()
                .scaledToFit()
                .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .opacity(0.6)
            Text("Ooops... \nYou have no cards. Please tap the \"+\" button to add new card.")
                .font(.custom("AvenirNext-DemiBold", size: 13))
                .multilineTextAlignment(.center)
                .foregroundColor(Color.gray)
                .padding([.leading, .trailing], 20)
        })
    }
}

struct NoTransactionsMessageView: View {
    var body: some View {
        VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 16, content: {
            Spacer()
            Image("fear")
                .resizable()
                .scaledToFit()
                .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .opacity(0.6)
            Text("Ooops... \nNo transactions added to this card. Please tap the button to add new transaction.")
                .font(.custom("AvenirNext-DemiBold", size: 13))
                .multilineTextAlignment(.center)
                .foregroundColor(Color.gray)
                .padding([.leading, .trailing], 20)
        })
    }
}
