//
//  RoundedButton.swift
//  Parco
//
//  Created by Marco Margarucci on 04/08/21.
//

import SwiftUI

struct RoundedButton: View {
    // Button text
    var text: String = ""
    // Button action
    var action: () -> Void
    
    var body: some View {
        Button(action: action, label: {
            ZStack {
                RoundedRectangle(cornerRadius: 16.0)
                    .frame(height: 50)
                    .frame(maxWidth: .infinity)
                    .foregroundColor(Color("clearBlue"))
                    .shadow(color: Color("gray").opacity(0.5), radius: 8.0, x: 0.0, y: 10.0)
                Text(text)
                    .font(.custom("AvenirNext-DemiBold", size: 16))
                    .frame(maxWidth: .infinity)
                    .frame(height: 50)
                    .foregroundColor(Color.white)
            }
        })
    }
}

struct RoundedButton_Previews: PreviewProvider {
    static var previews: some View {
        RoundedButton(text: "TEST", action: {})
    }
}
