//
//  LeadingTrailingTextButton.swift
//  Parco
//
//  Created by Marco Margarucci on 04/08/21.
//

import SwiftUI

struct LeadingTrailingTextButton: View {
    
    // MARK: - Properties
    // Button leading text
    var leadingText: String = ""
    // Button trailing text
    var trailingText: String = ""
    // Button action
    var action: () -> Void
    
    var body: some View {
        VStack(alignment: .leading, spacing: 16, content: {
            Button(action: action, label: {
                HStack(spacing: 4, content: {
                    Text(leadingText)
                        .font(.custom("AvenirNext-DemiBold", size: 14))
                        .foregroundColor(Color.white.opacity(0.8))
                    Text(trailingText)
                        .font(.custom("AvenirNext-Bold", size: 14))
                        .foregroundColor(Color("paleSiena"))
                })
            })
        })
    }
}
