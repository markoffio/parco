//
//  CreditCardView.swift
//  Parco
//
//  Created by Marco Margarucci on 10/08/21.
//

import SwiftUI

struct CardView: View {
    // MARK: - Properties
    // Card
    let card: Card
    // Delete card action sheet
    @State private var deleteCardSheetIsPresented: Bool = false
    // Used to check if we need to show the edit card form
    @State private var editCardFormIsPresented: Bool = false
    // UUID for card
    @State var id = UUID()
    // View context
    @Environment(\.managedObjectContext) private var viewContext
    
    // Fetch request
    var fetchRequest: FetchRequest<CardTransaction>

    init(card: Card) {
        self.card = card
        // Fetch card transactions
        fetchRequest = FetchRequest<CardTransaction>(entity: CardTransaction.entity(), sortDescriptors: [.init(key: "timestamp", ascending: false)], predicate: .init(format: "card == %@", self.card), animation: .linear)
    }
    
    // Date formatter
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        //formatter.dateStyle = .long
        formatter.dateFormat = "MMM y"
        return formatter
    }()
    
    // Delete selected card
    fileprivate func deleteCard() {
        // View context
        let viewContext = PersistenceController.shared.container.viewContext
        // Delete card
        viewContext.delete(card)
        
        do {
            try viewContext.save()
        } catch let error {
            debugPrint("Error deleting card: \(error.localizedDescription)")
        }
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 12) {
            HStack {
                Text(card.name ?? "")
                    .font(.custom("AvenirNext-DemiBold", size: 25))
                Spacer()
                // Ellipsis button
                Button(action: {
                    deleteCardSheetIsPresented.toggle()
                }, label: {
                    Image(systemName: "ellipsis")
                        .font(.system(size: 16, weight: .bold))
                })
                .actionSheet(isPresented: $deleteCardSheetIsPresented) {
                    .init(title: Text(self.card.name ?? "").font(.custom("AvenirNext-DemiBold", size: 16)),
                          message: Text("Options").font(.custom("AvenirNext-DemiBold", size: 13)),
                          buttons: [
                            .default(Text("Edit"), action: { editCardFormIsPresented.toggle() }),
                            .destructive(Text("Delete card"), action: deleteCard),
                            .cancel()])
                }
            }
            HStack {
                Image(card.image ?? "")
                    .resizable()
                    .scaledToFit()
                    .frame(height: 45)
                    .foregroundColor(.white)
                Spacer()
                // Compute balance
                if let balance = fetchRequest.wrappedValue.reduce(Float(card.balance ?? "") ?? 0.0, { $0 - $1.amount }) {
                    Text("Balance: \(String(format: "%.2f", balance)) €")
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                }
            }
            Text(String(card.number ?? ""))
                .font(.custom("AvenirNext-DemiBold", size: 15))
            HStack {
                Text("Credit limit: \(String(card.limit)) €")
                    .font(.custom("AvenirNext-DemiBold", size: 15))
                Spacer()
                Text("Valid thru: \(dateFormatter.string(from: card.expirationDate ?? Date()))")
                    .font(.custom("AvenirNext-DemiBold", size: 15))
            }
            HStack { Spacer() }
        }
        .foregroundColor(Color(.white))
        .padding()
        .background(Color("clearBlue"))
        .cornerRadius(16.0)
        .shadow(color: Color("gray").opacity(0.5), radius: 8.0, x: 0.0, y: 10.0)
        .padding(.horizontal)
        .fullScreenCover(isPresented: $editCardFormIsPresented, onDismiss: nil) {
            AddNewCardForm(card: self.card)
        }
    }
}


struct CreditCardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(card: Card())
    }
}

