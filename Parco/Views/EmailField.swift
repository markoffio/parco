//
//  EmailField.swift
//  Parco
//
//  Created by Marco Margarucci on 04/08/21.
//

import SwiftUI
import AudioToolbox

struct EmailField: View {
    // MARK: - Properties
    // Text string
    @Binding var email: String
    // Feedback genearator
    private let feedbackGenerator = UISelectionFeedbackGenerator()
    
    var body: some View {
        VStack(spacing: -5) {
            HStack(spacing: 12) {
                Image(systemName: "envelope")
                    .foregroundColor(Color.white)
                TextField("Email", text: $email)
                    .colorScheme(.dark)
                    .font(.custom("AvenirNext-DemiBold", size: 13))
                    .foregroundColor(Color.white)
                    .autocapitalization(.none)
                    .textContentType(.emailAddress)
            }
            .frame(height: 52)
            Rectangle()
                .frame(height: 1)
                .foregroundColor(Color.white.opacity(0.3))
        }
        .onTapGesture {
            // Generate haptic feedback
            feedbackGenerator.selectionChanged()
        }
    }
}

struct EmailField_Previews: PreviewProvider {
    static var previews: some View {
        EmailField(email: .constant("john@parco.com"))
    }
}
