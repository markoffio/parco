//
//  RoundedBarButton.swift
//  Parco
//
//  Created by Marco Margarucci on 10/08/21.
//

import SwiftUI

struct RoundedBarButton: View {
    // Button image name
    var imageName: String = ""
    // Button action
    var action: () -> Void
    
    var body: some View {
        Button(action: action, label: {
            ZStack {
                RoundedRectangle(cornerRadius: 8.0)
                    .frame(height: 35)
                    .frame(width: 35)
                    .foregroundColor(Color("lightBlue"))
                Image(systemName: imageName)
                    .resizable()
                    .frame(width: 15, height: 15)
                    .foregroundColor(Color("clearBlue"))
                    .font(Font.title.weight(.regular))
            }
        })
    }
}

struct RoundedBarButton_Previews: PreviewProvider {
    static var previews: some View {
        RoundedBarButton(imageName: "plus", action: {})
    }
}
