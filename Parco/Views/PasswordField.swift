//
//  PasswordField.swift
//  Parco
//
//  Created by Marco Margarucci on 04/08/21.
//

import SwiftUI
import AudioToolbox

struct PasswordField: View {
    // MARK: - Properties
    // Password string
    @Binding var password: String
    // Feedback genearator
    private let feedbackGenerator = UISelectionFeedbackGenerator()
    
    var body: some View {
        VStack(spacing: -5) {
            HStack(spacing: 12) {
                Image(systemName: "key")
                    .foregroundColor(Color.white)
                    .padding(.trailing, 6)
                SecureField("Password", text: $password)
                    .colorScheme(.dark)
                    .font(.custom("AvenirNext-DemiBold", size: 13))
                    .foregroundColor(Color.white)
                    .autocapitalization(.none)
                    .textContentType(.password)
            }
            .frame(height: 52)
            Rectangle()
                .frame(height: 1)
                .foregroundColor(Color.white.opacity(0.3))
        }
        .onTapGesture {
            // Generate haptic feedback
            feedbackGenerator.selectionChanged()
        }
    }
}

struct PasswordField_Previews: PreviewProvider {
    static var previews: some View {
        PasswordField(password: .constant("***********"))
    }
}
