//
//  RoundedBarButtonText.swift
//  Parco
//
//  Created by Marco Margarucci on 13/08/21.
//

import SwiftUI

struct RoundedBarButtonText: View {
    // Button text
    var text: String = ""
    // Button action
    var action: () -> Void
    
    var body: some View {
        Button(action: action, label: {
            ZStack {
                RoundedRectangle(cornerRadius: 8.0)
                    .frame(height: 35)
                    .frame(width: 60)
                    .foregroundColor(Color("clearBlue"))
                Text(text)
                    .font(.custom("AvenirNext-Bold", size: 13))
                    .foregroundColor(.white)
            }
        })
    }
}

struct RoundedBarButtonText_Previews: PreviewProvider {
    static var previews: some View {
        RoundedBarButtonText(text: "SAVE", action: {})
    }
}

