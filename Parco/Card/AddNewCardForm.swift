//
//  AddNewCardForm.swift
//  Parco
//
//  Created by Marco Margarucci on 10/08/21.
//

import SwiftUI
import CoreData

struct AddNewCardForm: View {
    // Environment variable
    @Environment(\.presentationMode) var presentationMode
    // Card name
    @State private var name: String = ""
    // Card number
    @State private var cardNumber: String = ""
    // Credit limit
    @State private var creditLimit: String = ""
    // Card type
    @State private var cardType: String = ""
    // Expiration date
    @State private var expirationDate =  Date()
    // Balance
    @State private var balance: String = ""
    // Image
    @State private var image: String = ""
    // Card type array
    private var cardTypeArray: [String] = ["VISA", "Mastercard", "American Express", "PayPal"]
    // Card number limit
    private var cardNumberLimit: Int = 19
    // Card
    let card: Card?
    
    var didAddCard: ((Card) -> ())? = nil
    
    init(card: Card? = nil, didAddCard: ((Card) -> ())? = nil) {
        self.card = card
        self.didAddCard = didAddCard
        
        _name = State(initialValue: self.card?.name ?? "")
        _cardNumber = State(initialValue: self.card?.number ?? "")
        if let limit = card?.limit {
            _creditLimit = State(initialValue: String(limit))
        }
        _cardType = State(initialValue: self.card?.type ?? "")
        _expirationDate = State(initialValue: self.card?.expirationDate ?? Date())
        _balance = State(initialValue: self.card?.balance ?? "")
        _image = State(initialValue: self.card?.image ?? "")
    }
            
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                // Back button
                HStack {
                    // Back button
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Image(systemName: "arrow.backward")
                            .resizable()
                            .frame(width: 22, height: 20)
                            .foregroundColor(Color("clearBlue"))
                            .font(Font.title.weight(.regular))
                    })
                    Text(self.card != nil ? "Edit card" : "Add new card")
                        .font(.custom("AvenirNext-DemiBold", size: 18))
                        .foregroundColor(Color("clearBlue"))
                        .padding(.leading, 16)
                    Spacer()
                    // Save button
                    RoundedBarButtonText(text: "SAVE") {
                        // View context
                        let viewContext = PersistenceController.shared.container.viewContext
                        // Create card entity
                        let card = self.card != nil ? self.card! : Card(context: viewContext)
                        card.timestamp = Date()
                        card.number = self.cardNumber
                        card.name = self.name
                        card.limit = Int32(self.creditLimit) ?? 0
                        card.expirationDate = self.expirationDate
                        card.type = self.cardType
                        card.balance = self.balance
                        switch card.type {
                        case "VISA":
                            card.image = "visa"
                        case "Mastercard":
                            card.image = "mastercard"
                        case "American Express":
                            card.image = "american-express"
                        case "PayPal":
                            card.image = "paypal"
                        default:
                            card.image = ""
                        }
                        do {
                            try viewContext.save()
                            presentationMode.wrappedValue.dismiss()
                            didAddCard?(card)
                        } catch {
                            debugPrint("Saving transaction error \(error.localizedDescription)")
                        }
                    }
                }
                .padding([.leading, .trailing], 20)
                .padding(.top, 10)
                // Form
                Form {
                    // Card information section
                    Section(header: Text("Card information").font(.custom("AvenirNext-DemiBold", size: 16))) {
                        TextField("Name", text: $name)
                            .font(.custom("AvenirNext-Medium", size: 15))
                        TextField("Balance", text: $balance)
                            .font(.custom("AvenirNext-Medium", size: 15))
                            .keyboardType(.numberPad)
                        TextField("Card number", text: $cardNumber)
                            .font(.custom("AvenirNext-Medium", size: 15))
                            .keyboardType(.numberPad)
                            .onChange(of: cardNumber, perform: { value in
                                if cardNumber.count == 4 || cardNumber.count == 9 || cardNumber.count == 14
                                { cardNumber += " " }
                            })
                            .onReceive(cardNumber.publisher.collect()) {
                                cardNumber = String($0.prefix(cardNumberLimit))
                            }
                        TextField("Limit", text: $creditLimit)
                            .font(.custom("AvenirNext-Medium", size: 15))
                            .keyboardType(.numberPad)
                        Picker("Type", selection: $cardType) {
                            ForEach(cardTypeArray, id: \.self) { cardType in
                                Text(String(cardType))
                                    .tag(String(cardType))
                                    .font(.custom("AvenirNext-DemiBold", size: 14))
                            }
                        }
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                    }
                    // Expiration date section
                    Section(header: Text("Expiration date").font(.custom("AvenirNext-DemiBold", size: 16))) {
                        DatePicker(selection: $expirationDate, in: Date()..., displayedComponents: .date) {
                            Text("Select a date")
                                .font(.custom("AvenirNext-DemiBold", size: 14))
                        }
                        .font(.custom("AvenirNext-DemiBold", size: 13))
                        .accentColor(Color("clearBlue"))
                    }
                }
                .navigationBarHidden(true)
            }
        }
    }
}

struct AddNewCreditCardView_Previews: PreviewProvider {
    static var previews: some View {
        AddNewCardForm()
    }
}
