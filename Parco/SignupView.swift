//
//  ContentView.swift
//  Parco
//
//  Created by Marco Margarucci on 03/08/21.
//

import SwiftUI
import AudioToolbox
import FirebaseAuth

struct SignupView: View {
    
    // MARK: - Properties
    // Email state variable
    @State private var email: String = ""
    // Password state variable
    @State private var password: String = ""
    // Feedback genearator
    private let feedbackGenerator = UISelectionFeedbackGenerator()
    // Used to check if we need to show the main view
    @State private var showMainView: Bool = false
    // Used to check if we need to show the sign up card
    @State private var showSignUpCard: Bool = true
    // Sign in with apple object
    @State private var signInWithAppleObject = SignInWithAppleObject()
    // Show alert view
    @State private var showAlertView: Bool = false
    // Alert title
    @State private var alertTitle: String = ""
    // Aler message
    @State private var alertMessage: String = ""
    
    var body: some View {
        ZStack {
            Image("background")
                .resizable()
                .scaledToFill()
                .edgesIgnoringSafeArea(.all)
            VStack {
                VStack(alignment: .leading, spacing: 16) {
                    Text("Parco")
                        .font(.custom("AvenirNext-Bold", size: 30))
                        .foregroundColor(.white)
                    Text(showSignUpCard ? "Sign up and start tracking your expenses" : "Please sign in")
                        .font(.custom("AvenirNext-DemiBold", size: 16))
                        .foregroundColor(Color.white.opacity(0.6))
                    // Email field
                    EmailField(email: $email)
                    // Password field
                    PasswordField(password: $password)
                    // Create account button
                    RoundedButton(text: showSignUpCard ? "Create account" : "Login") {
                        // Generate haptic feedback
                        feedbackGenerator.selectionChanged()
                        signup()
                        // Clear text fields
                        email = ""
                        password = ""
                    }
                    .onAppear {
                        Auth.auth().addStateDidChangeListener { auth, user in
                            // If the user exists we show the main view
                            if user != nil {
                                showMainView.toggle()
                            }
                        }
                    }
                    .fullScreenCover(isPresented: $showMainView) {
                        MainView()
                    }
                    if showSignUpCard {
                        // Privacy policy
                        Text("By clicking on \"Create account\" you agree to our Terms and Privacy Policy")
                            .font(.custom("AvenirNext-DemiBold", size: 12))
                            .multilineTextAlignment(.center)
                            .foregroundColor(Color.white.opacity(0.7))
                        // Divider
                        Rectangle()
                            .frame(height: 1)
                            .foregroundColor(Color.white.opacity(0.3))
                    }
                    
                    // Sign in/up button
                    LeadingTrailingTextButton(leadingText: showSignUpCard ? "Already have an account?" : "Don\'t have an account?" , trailingText: showSignUpCard ? "Sign in" : "Sign up") {
                        withAnimation(.easeInOut(duration: 0.3)) {
                            showSignUpCard.toggle()
                        }
                    }
                    if !showSignUpCard {
                        // Show forgot password button
                        LeadingTrailingTextButton(leadingText: "Forgot password?", trailingText: "Reset password") {
                            sendPasswordResetEmail()
                        }
                        // Divider
                        Rectangle()
                            .frame(height: 1)
                            .foregroundColor(Color.white.opacity(0.3))
                        // Sign in with Apple button
                        Button(action: {
                            signInWithAppleObject.signInWithApple()
                        }, label: {
                            SignInWithAppleButton()
                                .frame(height: 50)
                                .cornerRadius(16.0)
                        })
                    }
                }
                .padding(20)
            }
            .background(
                RoundedRectangle(cornerRadius: 25.0)
                    .stroke(Color.white.opacity(0.4))
                    .background(Color("blue"))
            )
            .cornerRadius(25.0)
            .padding(.horizontal)
            .shadow(color: Color("gray").opacity(0.5), radius: 20, x: 0.0, y: showSignUpCard ? 25.0 : -20.0)
            .alert(isPresented: $showAlertView) {
                Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .cancel())
            }
        }
        // Show main view
        /*
        .fullScreenCover(isPresented: $showMainView) {
            MainView()
        }
        */
    }
    
    // Sign up
    func signup() {
        if showSignUpCard {
            // Create user account
            Auth.auth().createUser(withEmail: email, password: password) { result, error in
                // Check for errors
                guard error == nil else {
                    self.alertTitle = "Create account error"
                    self.alertMessage = error!.localizedDescription
                    self.showAlertView.toggle()
                    return
                }
            }
        } else {
            // Sign in the user
            Auth.auth().signIn(withEmail: email, password: password) { result, error in
                guard error == nil else {
                    self.alertTitle = "Sign in error"
                    self.alertMessage = error!.localizedDescription
                    self.showAlertView.toggle()
                    return
                }
            }
        }
    }
    
    func sendPasswordResetEmail() {
        Auth.auth().sendPasswordReset(withEmail: email) { error in
            guard error == nil else {
                self.alertTitle = "Error sending password reset email"
                self.alertMessage = error!.localizedDescription
                self.showAlertView.toggle()
                return
            }
            alertTitle = "Password reset email sent"
            alertMessage = "Check your inbox to reset your password"
            showAlertView.toggle()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        SignupView()
    }
}
