//
//  CardTransactionView.swift
//  Parco
//
//  Created by Marco Margarucci on 18/08/21.
//

import SwiftUI

struct CardTransactionView: View {
    // MARK: - Properties
    // Delete transaction action sheet
    @State private var deleteTransactionSheetIsPresented: Bool = false
    // Transaction
    let transaction: CardTransaction
    
    // Date formatter
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none
        formatter.timeZone = .current
        return formatter
    }()
    
    // Delete selected transaction
    fileprivate func deleteTransaction() {
        // View context
        let viewContext = PersistenceController.shared.container.viewContext
        // Delete card
        viewContext.delete(transaction)
        
        do {
            try viewContext.save()
        } catch {
            debugPrint("Error deleting transaction: \(error.localizedDescription)")
        }
    }
    
    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 12) {
                VStack(alignment: .leading, spacing: 12) {
                    Text(transaction.name ?? "")
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                        .foregroundColor(Color("blueGrayDark"))
                    if let transactionTimestamp = transaction.timestamp {
                        Text(dateFormatter.string(from: transactionTimestamp))
                            .font(.custom("AvenirNext-DemiBold", size: 13))
                            .foregroundColor(Color("blueGrayDark").opacity(0.5))
                    }
                    Text(String(format: "%.2f", transaction.amount) + " €")
                        .font(.custom("AvenirNext-DemiBold", size: 13))
                        .foregroundColor(Color("blueGrayDark").opacity(0.5))
                }
            }
            .padding(20)
            Spacer()
            // Ellipsis button
            Button(action: {
                deleteTransactionSheetIsPresented.toggle()
            }, label: {
                Image(systemName: "ellipsis")
                    .font(.system(size: 16, weight: .bold))
                    .foregroundColor(Color("blueGrayDark").opacity(0.7))
            })
            .padding(.trailing, 20)
            .actionSheet(isPresented: $deleteTransactionSheetIsPresented) {
                .init(title: Text(self.transaction.name ?? "").font(.custom("AvenirNext-DemiBold", size: 16)),
                      message: Text("Do you want to delete \(self.transaction.name ?? "")?").font(.custom("AvenirNext-DemiBold", size: 13)),
                      buttons: [
                        .destructive(Text("Delete transaction"), action: deleteTransaction),
                        .cancel()])
            }
        }
    }
}

struct CardTransactionView_Previews: PreviewProvider {
    static var previews: some View {
        CardTransactionView(transaction: CardTransaction())
    }
}
