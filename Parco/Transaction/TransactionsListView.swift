//
//  TransactionListView.swift
//  Parco
//
//  Created by Marco Margarucci on 18/08/21.
//

import SwiftUI

struct TransactionsListView: View {
    // MARK: - Properties
    // Card
    let card: Card
    // View context
    @Environment(\.managedObjectContext) private var viewContext

    // Fetch request
    var fetchRequest: FetchRequest<CardTransaction>
    
    init(card: Card) {
        self.card = card
        // Fetch card transactions
        fetchRequest = FetchRequest<CardTransaction>(entity: CardTransaction.entity(), sortDescriptors: [.init(key: "timestamp", ascending: false)], predicate: .init(format: "card == %@", self.card), animation: .linear)
    }
        
    var body: some View {
        VStack {
            ForEach(fetchRequest.wrappedValue) { transaction in
                CardTransactionView(transaction: transaction)
                Divider()
                    .padding(.leading, 20)
            }
        }
    }
}

struct TransactionListView_Previews: PreviewProvider {
    static var previews: some View {
        TransactionsListView(card: Card())
    }
}
