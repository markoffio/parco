//
//  AddTransactionForm.swift
//  Parco
//
//  Created by Marco Margarucci on 17/08/21.
//

import SwiftUI
import CoreData

struct AddTransactionForm: View {
    // Card
    let card: Card
    // Environment variable
    @Environment(\.presentationMode) var presentationMode
    // Transaction date
    @State private var transactionDate =  Date()
    // Transaction name
    @State private var name: String = ""
    // Transcation amount
    @State private var amount: String = ""
    // Relation
    @State private var relation: String = ""
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                // Back button
                HStack {
                    // Back button
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Image(systemName: "arrow.backward")
                            .resizable()
                            .frame(width: 22, height: 20)
                            .foregroundColor(Color("clearBlue"))
                            .font(Font.title.weight(.regular))
                    })
                    Text("Add new transaction")
                        .font(.custom("AvenirNext-DemiBold", size: 18))
                        .foregroundColor(Color("clearBlue"))
                        .padding(.leading, 16)
                    Spacer()
                    // Save transaction button
                    RoundedBarButtonText(text: "SAVE") {
                        // View context
                        let viewContext = PersistenceController.shared.container.viewContext
                        // Transaction
                        let transaction = CardTransaction(context: viewContext)
                        transaction.name = self.name
                        transaction.amount = Float(self.amount) ?? 0.0
                        transaction.timestamp = self.transactionDate
                        transaction.card = self.card
                        // Save transaction
                        do {
                            try viewContext.save()
                            presentationMode.wrappedValue.dismiss()
                        } catch {
                            debugPrint("Saving transaction error \(error.localizedDescription)")
                        }
                    }
                }
                .padding([.leading, .trailing], 20)
                .padding(.top, 10)
                // Form
                Form {
                    // Card information section
                    Section(header: Text("Transaction information").font(.custom("AvenirNext-DemiBold", size: 16))) {
                        TextField("Name", text: $name)
                            .font(.custom("AvenirNext-Medium", size: 15))
                        TextField("Amount", text: $amount)
                            .font(.custom("AvenirNext-Medium", size: 15))
                            .keyboardType(.numberPad)
                        DatePicker(selection: $transactionDate, in: Date()..., displayedComponents: .date) {
                            Text("Date")
                                .font(.custom("AvenirNext-DemiBold", size: 14))
                        }
                        .font(.custom("AvenirNext-DemiBold", size: 13))
                        .accentColor(Color("clearBlue"))
                    }
                }
                .navigationBarHidden(true)
            }
        }
    }
}

struct AddTransactionForm_Previews: PreviewProvider {
    static var previews: some View {
        AddTransactionForm(card: Card())
    }
}
